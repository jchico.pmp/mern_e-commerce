const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
// const bcrypt = require('bcrypt');
const passport = require('passport');

//set up server

//initialize the app

const app = express();

const port = 3001;

//connect to database
mongoose.connect(
	'mongodb://localhost:27017/myappjoven',
	 {
	 	useNewUrlParser: true,
	 	useUnifiedTopology: true,
	 	
	 });
mongoose.connect('connected', () => {
	console.log("Database connected for real");
})


//use dependencies/middleware
app.use(bodyParser.json());
app.use(cors());
app.use('/public', express.static('public/products'));
app.use((req,res,next) => {
	console.log(req.body)
	next()
})
app.use(passport.initialize());



// Routes

app.use('/categories', require('./routes/categories'));
app.use('/products',require('./routes/products'));
app.use('/users', require('./routes/users.js'));
app.use('/transactions', require('./routes/transactions.js'));


//error handling middleware
app.use(function(err,req,res,next) {
		res.status(422).json({
				error : err.message,
		});
		// console.log(err);
})


app.listen(port, () => {
	console.log(`Listening to port ${port}`);
})

