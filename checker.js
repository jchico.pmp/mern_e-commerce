


const roleChecker = (req, res, next) => {
	if (req.user.role !== 'admin') {
		return res.send({ message: "You do not have access to this feature. Only for admin."})
	} else {
		next();
	};
}



module.exports = roleChecker;
