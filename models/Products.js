//load mongoose library for data modeling

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ProductSchema = new Schema({
	name : {
			type: String,
			required: [true, "Name field required"]
			},
	categoryId : {
			type: String,
			required: [true, "CategoryID field required"]
			},
	price : {
			type: Number,
			required: [true, "Price field required"]
			},
	description : {
			type: String,
			required: [true, "Description field required"]
			},
	image : {
			type: String,
			required: [true, "Image field required"]
			},
});

module.exports = mongoose.model('Product', ProductSchema);
