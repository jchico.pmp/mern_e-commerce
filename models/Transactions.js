const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TransactionSchema = new Schema ({
		userId : {
			type: String,
			required: true
		},
		transactionCode : {
			type: String,
			required: true,
			unique: true
		},
		dateCreated : {
			type: Date,
			default: Date.now
		},
		transactionDate : {
			type: Date,
			default: Date.now
		},
		status : {
			type: String,
			default: "Pending"
		},
		paymentMode : {
			type: String,
			default: "Over the Counter"
		},
		total : {
			type: Number,
			required: true

		},
		products: [
				{
			productId :String,
			quantity : Number,
			subtotal : Number,
			name: String
				}
				] 	
			

		
})

const Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = Transaction;