const express = require('express'); //pwede rin require('express').Router();
const router = express.Router();

const ProductModel = require('../models/Products');
const multer = require('multer');

const passport = require('passport');
const checker = require('./../checker.js');
const passportAuth = passport.authenticate('jwt', { session: false });

const storage = multer.diskStorage({
	destination : function(req, file, cb) {
		cb(null, "public/products")
	},
	filename : function(req, file, cb) {
		cb(null, Date.now() + '-' + file.originalname)
	}
})
const upload = multer({ storage : storage});


// yung dati ito --> const upload = multer({ dest : 'uploads/'});


//index
router.get('/', function(req,res, next){
	ProductModel.find().then(
		products => {
			res.json(products)
		}).catch(next)
});

//single  other method: findById ------> findById(req.params.id)
router.get('/:id', (req,res, next) => {
	ProductModel.findOne({
		_id : req.params.id
	}).then(
			product => res.json(product)).catch(next);
});


//create note: idagdag ang upload.single('name attribute in form') para sa upload photo
router.post('/', passportAuth, checker , upload.single('image') , (req,res, next) => {
	// res.json(req.file)

	checker.roleChecker;

	req.body.image = "/public/" + req.file.filename;
	ProductModel.create( req.body
		
	).then(
			product => res.send(product)).catch(next);


})

//put other method: findByIdAndUpdate -------> (req.params.id, req.body)
router.put('/:id',
	passportAuth,
	checker,
	upload.single('image'),
	(req,res, next) => {
	/*ProductModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		name: req.body.name,
		categoryId: req.body.categoryId,
		price: req.body.price,
		description: req.body.description,
		image: req.body.image
	},

	{
		new : true

	})*/

	// checker.roleChecker;

	let update = {
		...req.body
	}

	if(req.file) {
		update = {
			...req.body,
			image: "/public/" + req.file.filename
		}
	}

	// req.body.image = "/public/" + req.file.filename;


	ProductModel.findByIdAndUpdate( req.params.id, update, {new:true})

	.then(product => res.json(product))
		.catch(next)
})

//delete other method: deleteOne
router.delete('/:id',passportAuth, checker , (req,res, next) => {
	ProductModel.findOneAndDelete({
		_id : req.params.id
	}).then(product => res.json(product))
		.catch(next)
})

module.exports =  router;
