const express = require('express');
const router = express.Router();
const Product = require('./../models/Products.js');
const Transaction = require('./../models/Transactions.js');
const passport = require('passport');
const checker = require('./../checker.js');
const passportAuth = passport.authenticate('jwt', { session: false });
const User = require('./../models/Users.js');




/*

	order : [
			{ 
				id : "productID",
				qty : 5
			},
			{ 
				id : "productID",
				qty : 5
			},
			{ 
				id : "productID",
				qty : 5
			},
			{ 
				id : "productID",
				qty : 5
			}
		]

		products = [
							{
								id : "productID",
								quantity: order.qty * product.price
							}
							]

*/

router.get('/', passportAuth, (req, res, next) => {

	if(req.user.role == 'admin') {
	Transaction.find().then(transactions => {
		// res.send(transactions)

		User.find().then( users => {
			transactions.map(transaction => {
				users.forEach(user => {
					if(transaction.userId == user.id){
						transaction.userId = user.firstname+" "+user.lastname;
					}
				})


				return transaction
			})
			res.send(transactions)
		})
	})

		
	} else {
		Transaction.find({userId : req.user._id}).then(transactions => {

			User.find().then( users => {
			transactions.map(transaction => {
				users.forEach(user => {
					if(transaction.userId == user.id){
						transaction.userId = user.firstname+" "+user.lastname;
					}
				})


				return transaction
			})
			res.send(transactions)
		})


			})

	}
})

router.post('/orders', (req, res, next) => {
		// res.send(req.body)

		let orders = req.body.orders;

		// let orderIds = [];

		orderIds = orders.map( (product) => {
				return product.id;
		});

		// console.log(orderIds);

		Product.find( {
			_id: /*{$in : orderIds}*/ orderIds //<------pwede rin ito
		}).then( (products) => {
			let total = 0;

				let newProducts = products.map( (product) => {
					let matchedProduct = {};
					orders.forEach( (order) => {
						if (product.id === order.id) {
							// product.quantity = order.qty;
							// product.subtotal = order.qty * product.price;

							matchedProduct = {
								_id : product._id,
								name : product.name,
								price : product.price,
								image : product.image,
								quantity : order.qty,
								subtotal : order.qty * product.price
							};

						}
					})
					total += matchedProduct.subtotal;
					return matchedProduct;
				})

			res.send({
				products: newProducts,
				total
			});
		})
});

router.post('/',passportAuth, (req, res, next) => {
		

			let orders = req.body.orders;

		// let orderIds = [];

		orderIds = orders.map( (product) => {
				return product.id;
		});

		// console.log(orderIds);

		Product.find( {
			_id: /*{$in : orderIds}*/ orderIds //<------pwede rin ito
		}).then( (products) => {
			let total = 0;

				let newProducts = products.map( (product) => {
					let matchedProduct = {};
					orders.forEach( (order) => {
						if (product.id === order.id) {
							// product.quantity = order.qty;
							// product.subtotal = order.qty * product.price;

							matchedProduct = {
								productId : product._id,
								name : product.name,
								price : product.price,
								image : product.image,
								quantity : order.qty,
								subtotal : order.qty * product.price
							};

						}
					})
					total += matchedProduct.subtotal;
					return matchedProduct;
				})

				let transaction = {
									userId : req.user._id,
									transactionCode: Date.now(),
									total : total,
									products : newProducts
				}
				
				// return res.send(transaction);
				Transaction.create(transaction).then((transaction) => {
					return res.send(transaction);
				})		
		})
})


router.put('/:id', passportAuth, checker , (req, res, next) => {
					
					checker.roleChecker;

					Transaction.findByIdAndUpdate(
						req.params.id,
						{
							status : req.body.status
						},
						{
							new : true
						}). then( (transaction) => {
							res.send(transaction)
						})
})


module.exports = router;